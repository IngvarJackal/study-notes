# [Quadratics](https://www.khanacademy.org/math/algebra/quadratics)


## Factorization
[Factorization](https://en.wikipedia.org/wiki/Factorization) or *factoring*
consists of writing a number or another mathematical object as a product of
several factors, usually smaller or simpler objects of the same kind.

Formula: $`ax^2 + bx + c`$, where $`b = k_1 + k_2,\ a * c = k_1 * k_2`$