[Functions, Vertical Line Test, Ordered Pairs, Tables, Domain and Range](https://youtu.be/DrEXTC6mIO8)

## Vertical line test in table form

![](notes/precalculus/files/general/vertical_line_test_table_form.png)

All X values in a function are unique

---

## Function Range and Domain

![](notes/precalculus/files/general/domain_range_function.png)

Function **domain** is a set of all X values

Function **range** is a set of all Y values

![](notes/precalculus/files/general/domain_range_function_analytical.png)

To find function **domain** from a formula, find an union of all constraints for X value. For example, if X is in a divisor, the divisor can't be 0;
or if X is inside a root with an **even** degree, then the root can't be negative.

---

## Vertical line test

![](notes/precalculus/files/general/vertical_line_test.png)

---

## Piecewise function

![](notes/precalculus/files/general/piecewise_function.png)

---

## Graphing piecewise function

![](notes/precalculus/files/general/graphing_piecewise_function.png)