# [Difference Quotient](https://youtu.be/qQgVomi8lCc)
Difference quotient is a measure of the average rate of change of the function over an interval (h)

![](notes/precalculus/files/difference_quotient/difference_quotient.png)

## x
![](notes/precalculus/files/difference_quotient/difference_quotient_x.png)

## x + C
![](notes/precalculus/files/difference_quotient/difference_quotient_x+c.png)

## x^2
![](notes/precalculus/files/difference_quotient/difference_quotient_x^2.png)

## sqrt(x)
![](notes/precalculus/files/difference_quotient/difference_quotient_sqrt(x)_A.png)
![](notes/precalculus/files/difference_quotient/difference_quotient_sqrt(x)_B.png)

## 1/x
![](notes/precalculus/files/difference_quotient/difference_quotient_x^-1_A.png)
![](notes/precalculus/files/difference_quotient/difference_quotient_x^-1_B.png)

## poly(x)
![](notes/precalculus/files/difference_quotient/difference_quotient_polynom_A.png)
![](notes/precalculus/files/difference_quotient/difference_quotient_polynom_B.png)