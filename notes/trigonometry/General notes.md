![Evaluating Trigonometric Functions Given a Point on the Terminal Side Trigonometry](https://youtu.be/P8rdY2ahkms)

![](notes/trigonometry/files/general/func_formulas.png)

![](notes/trigonometry/files/general/quadrant_signs.png)

**Mnemonic Rule**: All students take calculus: 1Q -- all positive, 2Q -- sin, 3Q -- tan, 4Q -- cos

![](notes/trigonometry/files/general/func_examples.png)