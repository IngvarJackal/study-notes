# Special triangles
<img src="notes/trigonometry/files/triangles/special_right_triangle.png" width="256" class=".center">
<img src="notes/trigonometry/files/triangles/special_right_triangle2.png" width="256">


# Laws of sines and cosines
<img src="notes/trigonometry/files/triangles/laws_of_sines_cosines.png" width="256">

Law of sines: $`\frac{A}{sin(a)} = \frac{B}{sin(b)} = \frac{C}{sin(c)}`$

Law of cosines: $`C^2 = A^2 + B^2 - 2ABcos(c)`$

# Proof of law of sines

<div align="center">
<img src="notes/trigonometry/files/triangles/law_of_sines_proof.png" width="256">
</div>

```math
\begin{aligned}
(1)\ & sin(a) = \frac{x}{B} \\
     & x = Bsin(a) \\
(2)\ & sin(b) = \frac{x}{A} \\
     & x = Asin(b) \\
(3)\ & x = Asin(b) = Bsin(A)  \\
(4)\ & sin(b) = \frac{Bsin(A)}{A} \\
(5)\ & \frac{sin(b)}{B} = \frac{sin(A)}{A} \ | \ ^{-1} \\
(6)\ & \frac{A}{sin(a)} = \frac{B}{sin(B)} \\
\end{aligned}
```

# Proof of law of cosines

<div align="center">
<img src="notes/trigonometry/files/triangles/law_of_cosines_proof.png" width="256">
</div>

```math
\begin{aligned}
(1)\ & cos(a) = \frac{D}{B} \\
     & D = Bcos(a) \\
(2)\ & E = C - D = C - Bcos(a) \\
\\
(3)\ & \frac{M}{B} = sin(a)  \\
     & M = Bsin(a) \\ 
\\
(4)\ & A^2 = M^2 + E^2 \\
\\
(5)\ & A = (Bsin(a))^2 + (C-  Bcos(a))^2 \\
  &\ \ \ = B^2sin^2(a) + C^2 - 2BCcos(a) + B^2cos^2(a) \\
  &\ \ \ = B^2(sin^2(a) + cos^2(a)) + C^2 - 2BCcos(a) \ | \ _{sin^2(a) + cos^2(a) = 1} \\
\\
(6) & A = B^2 + C^2 - 2BCcos(a) \\
\end{aligned}
```